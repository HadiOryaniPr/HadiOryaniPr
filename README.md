<h1 align="center">Hi 🤟, I'm Hadi Oryani</h1>
<h3 align="center">A FrontEnd Developer</h3>

<p align="left"> <img src="https://komarev.com/ghpvc/?username=hadioryanipr&label=Profile%20views&color=0e75b6&style=flat" alt="hadioryanipr" /> </p>

- 🤔 What am I focusing on **Java Script**

- 👨‍💻 All of my projects are available at [https://github.com/HadiOryaniPr?tab=repositories](https://github.com/HadiOryaniPr?tab=repositories)

- ❓ Ask me about **HTML / CSS / SASS** 

- 📫 How to reach me **hadioryanipr@gmail.com**

- 🤘 **love game & and think i am funny Because others say**

<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://linkedin.com/in/hadi-oryani-917544219/" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="hadi-oryani-917544219/" height="30" width="40" /></a>
<a href="https://stackoverflow.com/users/16299432/hadioryani" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/stack-overflow.svg" alt="16299432/hadioryani" height="30" width="40" /></a>
<a href="https://instagram.com/hadioryanipr" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="hadioryanipr" height="30" width="40" /></a>
<a href="https://medium.com/@hadioryanipr" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/medium.svg" alt="@hadioryanipr" height="30" width="40" /></a>
</p>
 

<h3 align="left">Languages and Tools:</h3>
<p align="left"> <a href="https://www.w3schools.com/css/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> <a href="https://sass-lang.com" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/sass/sass-original.svg" alt="sass" width="40" height="40"/> </a> </p>

   
<p>&nbsp;<img align="center" src="https://github-readme-stats.vercel.app/api?username=hadioryanipr&show_icons=true&locale=en" alt="hadioryanipr" /></p>


<p><img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=hadioryanipr&" alt="hadioryanipr" /></p>


<p><img align="left" src="https://github-readme-stats.vercel.app/api/top-langs?username=hadioryanipr&show_icons=true&locale=en&layout=compact" alt="hadioryanipr" /></p>
 
    
<h3 align="left">Support:</h3>
<p><a href="https://www.buymeacoffee.com/HadiOryaniPr"> <img align="left" src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" height="50" width="210" alt="HadiOryani" /></a></p>
 
    
 
